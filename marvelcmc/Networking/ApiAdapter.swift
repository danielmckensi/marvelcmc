//
//  ApiAdapter.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 10/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation
import Alamofire

class ApiAdapter {
    
    static let get = ApiAdapter()
    
    private let alamofireManager: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        let sessionManager = Alamofire.Session(configuration: configuration)
        return sessionManager
    }()
    
    private init() {
        
    }
    
    func requestGeneric<T: Decodable>(url: String, method: Alamofire.HTTPMethod = .get,
                                        queryParams: [String: String] = [:], body: Parameters? = nil,
                                        headers: HTTPHeaders? = nil, completionHandler: @escaping (AFDataResponse<T>) -> Void) {
        var urlComponent = URLComponents(string: url)!
        
        if queryParams.count > 0 {
            let queryItems = queryParams.map { URLQueryItem(name: $0.key, value: $0.value) }
            urlComponent.queryItems = queryItems
        }
                
        alamofireManager.request(urlComponent, method: method, parameters: body, encoding: JSONEncoding.prettyPrinted, headers: headers).responseDecodable(completionHandler: completionHandler)
    }
    
}

//extension DataRequest {
//
//    private func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
//        return DataResponseSerializer { _, response, data, error in
//            if error != nil {
//                //TODO: remove FarmatodoManager from this place
//                if let err = error as? URLError, err.code == URLError.Code.notConnectedToInternet {
//                    FarmatodoManager.isNetworkReachable = "false"
//                }
//                return .failure(error!)
//            }
//            FarmatodoManager.isNetworkReachable = "true"
//
//            guard let data = data else {
//                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
//            }
//
//            return Alamofire.Result { try JSONDecoder().decode(T.self, from: data) }
//        }
//    }
//
//    @discardableResult
//    func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (AFDataResponse<T>) -> Void) -> Self {
//        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
//    }
//}

extension DataResponse {
    func getJsonBody() -> String {
        return NSString(data: (self.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue)! as String
    }
}


