//
//  MarvelManager.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 10/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation

class MarvelManager : NSObject{
    static let get  = MarvelManager()
    var marvelService = MarvelServices.get
    
    private override init() {
        
    }
    
    func getComicList(responseValue: @escaping (ComicListRes) -> Void, onFailure: (() -> Void)? = nil) {
        marvelService.getComics(responseValue: responseValue, onFailure: onFailure)
    }
    
    func getCharactersList(responseValue: @escaping (CharactersListRes) -> Void, onFailure: (() -> Void)? = nil) {
        marvelService.getCharacters(responseValue: responseValue, onFailure: onFailure)
    }
    
    func getSeriesList(responseValue: @escaping (SeriesListRes) -> Void, onFailure: (() -> Void)? = nil) {
        marvelService.getSeries(responseValue: responseValue, onFailure: onFailure)
    }
}
