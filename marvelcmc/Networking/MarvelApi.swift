//
//  MarvelApi.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 10/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation

class MarvelApi {
    private init(){
        
    }
    
    static let getComics = "https://gateway.marvel.com/v1/public/comics"
    static let getCharacters = "https://gateway.marvel.com/v1/public/characters"
    static let getSeries = "https://gateway.marvel.com/v1/public/series"
    
}
