//
//  CharactersListRes.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 10/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation

// MARK: - ComicListRes
class CharactersListRes: Codable {
    let code: Int?
    let status, copyright, attributionText, attributionHTML: String?
    let etag: String?
    let data: CharacterResult?
}

class CharacterResult: Codable {
    let offset, limit, total, count: Int
    let results: [Character]
    
}

class Character: Codable{
    let id: Int
    let name: String
    let thumbnail: Thumbnail
    let description: String
    let comics: ComicDetail
}

class ComicDetail: Codable{
    let items: [MarvelItem]
}

class MarvelItem: Codable{
    var resourceURI: String
    var name: String
}
