//
//  ComicListRes.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 10/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let comicListRes = try? newJSONDecoder().decode(ComicListRes.self, from: jsonData)

import Foundation

// MARK: - ComicListRes
class ComicListRes: Codable {
    let code: Int?
    let status, copyright, attributionText, attributionHTML: String?
    let etag: String?
    let data: ComicResult?
}

// MARK: - DataClass
class ComicResult: Codable {
    let offset, limit, total, count: Int
    let results: [Comic]
    
}

class Comic: Codable{
    let id: Int
    let title: String
    let thumbnail: Thumbnail
    let creators: CreatorList
}

class Thumbnail: Codable {
    var path: String
    var extensionUrl: String
    
    enum CodingKeys: String, CodingKey {
        case path
        case extensionUrl = "extension"
    }
    init(path: String, extensionUrl: String) {
        self.path = path
        self.extensionUrl = extensionUrl
    }
}
