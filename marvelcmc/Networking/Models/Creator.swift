//
//  Creator.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 11/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation

class CreatorList: Codable{
    var items: [Creator]
}
class Creator : Codable {
    var resourceURI : String
    var name: String
    var role: String
}
