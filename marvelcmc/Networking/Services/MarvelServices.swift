//
//  MarvelService.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 10/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation
import Alamofire

class MarvelServices {
    static let get = MarvelServices()
    
    private init(){
        
    }
    
    func getComics(responseValue: @escaping (ComicListRes) -> Void, onFailure: (() -> Void)? = nil) {
        let queryParams = [
            "ts":"1",
            "apikey":"67252c8d4c73bc728e0c220f2133a289",
            "hash":"019d73c235ab2b23f56b726dd5771e5e"
        ]
        
         ApiAdapter.get.requestGeneric(url: MarvelApi.getComics, queryParams: queryParams) { (response: AFDataResponse<ComicListRes>) in
             if let comics = response.value {
                 responseValue(comics)
             } else {
                 print("getComics error -> \(response.error)")
                 onFailure?()
             }
         }
     }
    
    func getCharacters(responseValue: @escaping (CharactersListRes) -> Void, onFailure: (() -> Void)? = nil) {
        let queryParams = [
            "ts":"1",
            "apikey":"67252c8d4c73bc728e0c220f2133a289",
            "hash":"019d73c235ab2b23f56b726dd5771e5e"
        ]
        
         ApiAdapter.get.requestGeneric(url: MarvelApi.getCharacters, queryParams: queryParams) { (response: AFDataResponse<CharactersListRes>) in
             if let comics = response.value {
                 responseValue(comics)
             } else {
                 print("getComics error -> \(response.error)")
                 onFailure?()
             }
         }
     }
    
    func getSeries(responseValue: @escaping (SeriesListRes) -> Void, onFailure: (() -> Void)? = nil) {
         let queryParams = [
             "ts":"1",
             "apikey":"67252c8d4c73bc728e0c220f2133a289",
             "hash":"019d73c235ab2b23f56b726dd5771e5e"
         ]
         
          ApiAdapter.get.requestGeneric(url: MarvelApi.getSeries, queryParams: queryParams) { (response: AFDataResponse<SeriesListRes>) in
              if let comics = response.value {
                  responseValue(comics)
              } else {
                  print("getComics error -> \(response.error)")
                  onFailure?()
              }
          }
      }
     
}
