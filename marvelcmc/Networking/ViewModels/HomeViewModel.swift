//
//  HomeViewModel.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 10/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation

class HomeViewModel {
    var marvelManager = MarvelManager.get
    var listenerComicList: ((ComicListRes) -> Void)?
    var listenerCharactersList : ((CharactersListRes) -> Void)?
    var listenerSeriesList : ((SeriesListRes) -> Void)?
    var listenerOnFailure: (() -> Void)?
    
    func getComics() {
        if let responseValue = listenerComicList {
            marvelManager.getComicList(responseValue: responseValue, onFailure: listenerOnFailure)
        }
    }
    
    func getCharacters() {
          if let responseValue = listenerCharactersList {
              marvelManager.getCharactersList(responseValue: responseValue, onFailure: listenerOnFailure)
          }
      }
    
    func getSeries() {
          if let responseValue = listenerSeriesList {
              marvelManager.getSeriesList(responseValue: responseValue, onFailure: listenerOnFailure)
          }
      }
}
