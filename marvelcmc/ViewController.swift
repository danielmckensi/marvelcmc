//
//  ViewController.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 10/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import UIKit
import GlidingCollection
import Kingfisher
import SVProgressHUD

class ViewController: UIViewController {
    let items = ["comics", "personajes", "series"]
    
    @IBOutlet weak var charactersCollection: UICollectionView!
    @IBOutlet var glidingCollection: GlidingCollection!
    
    var viewModel = HomeViewModel()
    
    var comics : ComicListRes?
    var characters : CharactersListRes?
    var series: SeriesListRes?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initListener()
        SVProgressHUD.show()
        viewModel.getComics()
        setUpUI()
    }
    
    func setUpUI(){
        glidingCollection.dataSource = self
        glidingCollection.collectionView.dataSource = self
        glidingCollection.collectionView.delegate = self
        
        charactersCollection.delegate = self
        charactersCollection.dataSource = self
        
        glidingCollection.collectionView.backgroundColor = .clear
        glidingCollection.backgroundColor = .clear
        
        glidingCollection.collectionView.registerNib("MoviesCollectionViewCell")
        charactersCollection.registerNib("CharactersCollectionViewCell")
        
        let cellSize = CGSize(width:100 , height:100)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal //.horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.minimumLineSpacing = 0.2
        layout.minimumInteritemSpacing = 0.2
        charactersCollection.setCollectionViewLayout(layout, animated: true)
        
        
        var config = GlidingConfig.shared
        config.buttonsFont = UIFont.boldSystemFont(ofSize: 28)
        config.activeButtonColor = .black
        config.inactiveButtonsColor = .lightGray
        config.cardShadowColor = .black
        config.cardShadowOffset = CGSize(width: -2, height: -2)
        config.cardShadowRadius = 10
        GlidingConfig.shared = config
    }
    
    func initListener(){
        viewModel.listenerComicList = { [weak self] comics in
            guard let strongSelf = self else {return}
            strongSelf.comics = comics
            strongSelf.viewModel.getCharacters()
            
        }
        
        viewModel.listenerCharactersList = { [weak self] characters in
            guard let strongSelf = self else {return}
            strongSelf.characters = characters
            strongSelf.viewModel.getSeries()
            strongSelf.charactersCollection.reloadData()
        }
        
        viewModel.listenerSeriesList = { [weak self] series in
            guard let strongSelf = self else {return}
            strongSelf.series = series
            strongSelf.glidingCollection.collectionView.reloadData()
            SVProgressHUD.dismiss()
        }
    }
}

extension ViewController: GlidingCollectionDatasource {
    
    func numberOfItems(in collection: GlidingCollection) -> Int {
        return items.count
    }
    
    func glidingCollection(_ collection: GlidingCollection, itemAtIndex index: Int) -> String {
        return "– " + items[index]
    }
    
}

extension ViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == glidingCollection.collectionView{
            let section = glidingCollection.expandedItemIndex
            switch section {
            case 0:
                let vc = ComicDetailViewController()
                vc.comic = comics?.data?.results[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            case 1:
                let character = characters?.data?.results[indexPath.row]
                let vc = CharacterDetailViewController()
                vc.character = character
                self.navigationController?.pushViewController(vc, animated: true)
            case 2:
                let vc = ComicDetailViewController()
                vc.comic = series?.data?.results[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
                print("selected a series")
            default:
                break
            }
        }else{
            let character = characters?.data?.results[indexPath.row]
            let vc = CharacterDetailViewController()
            vc.character = character
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}
extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == glidingCollection.collectionView{
            return comics?.data?.results.count ?? 0
        }else{
            return characters?.data?.count ?? 0
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == glidingCollection.collectionView{
            let section = glidingCollection.expandedItemIndex
            switch section {
            case 0:
                let comicImagePath = comics?.data?.results[indexPath.row].thumbnail.path ?? ""
                let comicImageExtension = comics?.data?.results[indexPath.row].thumbnail.extensionUrl ?? ""
                let comicImageUrl = ("\(comicImagePath).\(comicImageExtension)")
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoviesCollectionViewCell", for: indexPath) as? MoviesCollectionViewCell else { return UICollectionViewCell() }
                cell.imageView.tag = kGlidingCollectionParallaxViewTag
                
                if let url = URL(string: comicImageUrl) {
                    cell.imageView?.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, URL) in
                        cell.setNeedsLayout()
                    })
                }
                return cell
            case 1:
                let comicImagePath = characters?.data?.results[indexPath.row].thumbnail.path ?? ""
                let comicImageExtension = characters?.data?.results[indexPath.row].thumbnail.extensionUrl ?? ""
                let comicImageUrl = ("\(comicImagePath).\(comicImageExtension)")
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoviesCollectionViewCell", for: indexPath) as? MoviesCollectionViewCell else { return UICollectionViewCell() }
                cell.imageView.tag = kGlidingCollectionParallaxViewTag
                
                if let url = URL(string: comicImageUrl) {
                    cell.imageView?.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, URL) in
                        cell.setNeedsLayout()
                    })
                }
                return cell
            case 2:
                let comicImagePath = series?.data?.results[indexPath.row].thumbnail.path ?? ""
                let comicImageExtension = series?.data?.results[indexPath.row].thumbnail.extensionUrl ?? ""
                let comicImageUrl = ("\(comicImagePath).\(comicImageExtension)")
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoviesCollectionViewCell", for: indexPath) as? MoviesCollectionViewCell else { return UICollectionViewCell() }
                cell.imageView.tag = kGlidingCollectionParallaxViewTag
                
                if let url = URL(string: comicImageUrl) {
                    cell.imageView?.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, URL) in
                        cell.setNeedsLayout()
                    })
                }
                return cell
            default:
                return UICollectionViewCell()
            }
        }
        else{
            let charactersCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CharactersCollectionViewCell", for: indexPath) as? CharactersCollectionViewCell
            let comicImagePath = characters?.data?.results[indexPath.row].thumbnail.path ?? ""
            let comicImageExtension = characters?.data?.results[indexPath.row].thumbnail.extensionUrl ?? ""
            let comicImageUrl = ("\(comicImagePath).\(comicImageExtension)")
            
            if let url = URL(string: comicImageUrl) {
                charactersCell?.imgView?.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, URL) in
                    charactersCell?.setNeedsLayout()
                })
            }
            return charactersCell ?? UICollectionViewCell()
        }
        
    }
    
}
