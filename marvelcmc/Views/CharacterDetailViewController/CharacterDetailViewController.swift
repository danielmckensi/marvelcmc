//
//  ComicDetailViewController.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 10/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import UIKit
import Kingfisher

class CharacterDetailViewController: UIViewController {
    
    var character : Character?
    
    @IBOutlet weak var tblView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    init(){
        super.init(nibName: "CharacterDetailViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpUI(){
        tblView.delegate = self
        tblView.dataSource = self
        tblView.registerNib("HeaderDetailTableViewCell")
        tblView.registerNib("DescriptionDetailTableViewCell")
        tblView.registerNib("CharactersAppearTableViewCell")
    }
}

extension CharacterDetailViewController : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return character?.comics.items.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = HeaderCarruselView.instanceFromNib() as? HeaderCarruselView
        header?.lblTitle.text = "Aparece en"
        return header ?? UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        case 1:
            return 80
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
                  case 0:
                      let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderDetailTableViewCell") as? HeaderDetailTableViewCell
                      let characterImagePath = character?.thumbnail.path ?? ""
                      let characterExtension = character?.thumbnail.extensionUrl ?? ""
                      let characterImageUrl = ("\(characterImagePath).\(characterExtension)")
                      
                      if let url = URL(string: characterImageUrl) {
                          cell?.imgView?.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, URL) in
                              cell?.setNeedsLayout()
                          })
                      }
                      
                      if let name = character?.name {
                          cell?.lblTitle.text = name
                      }
                      
                      return cell ?? UITableViewCell()
                  case 1:
                      let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionDetailTableViewCell") as? DescriptionDetailTableViewCell
                      if let description = character?.description {
                          cell?.lblDescription.text = description
                      }
                      return cell ?? UITableViewCell()
                  default:
                      return UITableViewCell()
                  }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CharactersAppearTableViewCell") as? CharactersAppearTableViewCell
            let nameComic = character?.comics.items[indexPath.row].name
            cell?.lblTitle.text = nameComic
            return cell ?? UITableViewCell()
        default:
            return UITableViewCell()
        }
      
    }
    
    
    
}

extension CharacterDetailViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                return 400
            case 1:
                return tableView.estimatedRowHeight
            default:
                return 0
            }
        case 1:
            return tableView.estimatedRowHeight
        default:
            return 0
        }
    }
}
