//
//  CharactersCollectionViewCell.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 11/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import UIKit

class CharactersCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.layer.cornerRadius = imgView.bounds.width / 2
        // Initialization code
    }

}
