//
//  MoviesCollectionViewCell.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 10/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import UIKit

class MoviesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
