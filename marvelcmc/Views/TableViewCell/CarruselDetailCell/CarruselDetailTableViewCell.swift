//
//  CarruselDetailTableViewCell.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 11/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import UIKit

class CarruselDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    var items : [MarvelItem]?
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerNib("CarruselCollectionViewCell")
        // Initialization code
    }
}

extension CarruselDetailTableViewCell : UICollectionViewDelegate{
    
}

extension CarruselDetailTableViewCell : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarruselCollectionViewCell", for: indexPath) as? CarruselCollectionViewCell
        cell?.lblTitle.text = items?[indexPath.row].name
        return cell ?? UICollectionViewCell()
    }
}

extension CarruselDetailTableViewCell : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
}
