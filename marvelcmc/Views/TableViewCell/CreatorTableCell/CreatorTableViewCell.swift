//
//  CreatorTableViewCell.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 11/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import UIKit

class CreatorTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblRole: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
