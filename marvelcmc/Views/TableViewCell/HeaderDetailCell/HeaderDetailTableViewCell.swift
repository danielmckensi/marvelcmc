//
//  HeaderDetailTableViewCell.swift
//  marvelcmc
//
//  Created by Daniel Steven Murcia Almanza on 11/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import UIKit

class HeaderDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpUI()
    }
    
    func setUpUI(){
        imgView.layer.cornerRadius = 10
    }
}
